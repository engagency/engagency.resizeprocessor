﻿using Sitecore.Resources.Media;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Configuration;
using Sitecore.Diagnostics;

namespace Engagency.Resources.Media
{
    /// <summary>
    /// ResizeProcessor class
    /// 
    /// </summary>
    public class ResizeProcessor
    {
            /// <summary>
            /// Runs the processor.
            /// 
            /// </summary>
            /// <param name="args">The arguments.</param>
            public void Process(GetMediaStreamPipelineArgs args)
            {
                Assert.ArgumentNotNull((object)args, "args");
                if (args.Options.Thumbnail)
                    return;
                MediaStream outputStream = args.OutputStream;
                if (outputStream == null)
                    return;
                if (!outputStream.AllowMemoryLoading)
                {
                    Tracer.Error((object)"Could not resize image as it was larger than the maximum size allowed for memory processing. Media item: {0}", (object)outputStream.MediaItem.Path);
                }
                else
                {
                    if (!args.MediaData.MimeType.StartsWith("image/", StringComparison.Ordinal))
                        return;
                    string extension = args.MediaData.Extension;
                    ImageFormat imageFormat = this.GetImageFormat(extension, (ImageFormat)null);
                    if (imageFormat == null)
                        return;
                    TransformationOptions transformationOptions = args.Options.GetTransformationOptions();
                    if (!transformationOptions.ContainsResizing())
                        return;
                    this.ApplyBackgroundColor(args, imageFormat, transformationOptions);
                    MediaStream mediaStream = outputStream;
                    Stream stream = MediaManager.Effects.TransformImageStream(mediaStream.Stream, transformationOptions, imageFormat);
                    args.OutputStream = new MediaStream(stream, extension, mediaStream.MediaItem);
                }
            }

            /// <summary>
            /// Applies the color of the background.
            /// 
            /// </summary>
            /// <param name="args">The arguments.</param><param name="imageFormat">The image format.</param>
            /// <param name="transformationOptions">The transformation options.</param>
            protected virtual void ApplyBackgroundColor(GetMediaStreamPipelineArgs args, ImageFormat imageFormat, TransformationOptions transformationOptions)
            {
                Assert.ArgumentNotNull((object)args, "args");
                Assert.ArgumentNotNull((object)imageFormat, "imageFormat");
                Assert.ArgumentNotNull((object)transformationOptions, "transformationOptions");
                //if (!transformationOptions.BackgroundColor.IsEmpty || imageFormat != ImageFormat.Bmp && imageFormat != ImageFormat.Gif && imageFormat != ImageFormat.Jpeg)
                if (imageFormat != ImageFormat.Bmp && imageFormat != ImageFormat.Gif && imageFormat != ImageFormat.Jpeg)
                    return;
                transformationOptions.BackgroundColor = Settings.Media.DefaultImageBackgroundColor;
            }

        /// <summary>
        /// Gets the image format for a specific extension.
        /// 
        /// </summary>
        /// <param name="extension">The file extension describing the media.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>
        /// The Image Format.
        /// </returns>
        public virtual ImageFormat GetImageFormat(string extension, ImageFormat defaultValue)
        {
            Assert.ArgumentNotNull((object) extension, "extension");
            switch (extension.ToLowerInvariant())
            {
                case "bmp":
                    return ImageFormat.Bmp;
                case "emf":
                    return ImageFormat.Emf;
                case "gif":
                    return ImageFormat.Png; // was ImageFormat.Gif; Hack to get resizing to work on 64-bit app pools
                case "ico":
                case "icon":
                    return ImageFormat.Icon;
                case "jpg":
                case "jpeg":
                    return ImageFormat.Png; // .jpg format seems to have issues too...
                case "png":
                    return ImageFormat.Png;
                case "tiff":
                case "tif":
                    return ImageFormat.Tiff;
                case "wmf":
                    return ImageFormat.Wmf;
                default:
                    return defaultValue;
            }
        }
    }
}
